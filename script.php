<?php

$dbhost = getenv('dbhost') ? getenv('dbhost') : 'undefined';
$dbname = getenv('dbname') ? getenv('dbname') : 'undefined';
$dbuser = getenv('dbuser') ? getenv('dbuser') : 'undefined';
$dbpass = getenv('dbpass') ? getenv('dbpass') : 'undefined';
$api_key_test = getenv('api_key_test') ? getenv('api_key_test') : 'undefined';

echo "Loading env variables..." . PHP_EOL . PHP_EOL;
echo "Modificación" . PHP_EOL . PHP_EOL;

echo "dbhost: $dbhost" . PHP_EOL;
echo "dbname: $dbname" . PHP_EOL;
echo "dbuser: $dbuser" . PHP_EOL;
echo "dbpass: $dbpass" . PHP_EOL;
echo "api_key_test: $api_key_test" . PHP_EOL;
